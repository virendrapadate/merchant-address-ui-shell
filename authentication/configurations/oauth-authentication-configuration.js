'use strict';

const config = require('config');
const axios = require('axios');

const logger = require('../../logging/logger');

const oAuthAuthenticationConfiguration = {
    provider: {
        protocol: 'oauth2',
        useParamsAuth: true,
        auth: `${config.get('authentication.uaa.baseURL')}/oauth/authorize`,
        token: `${config.get('authentication.uaa.baseURL')}/oauth/token`,
        scope: ['uaa.resource', 'openid'],
        profile: (credentials, params, get, callback) =>
        {
            logger.debug('fetching user profile');
            const userinfoURL = `${config.get('authentication.uaa.baseURL')}/userinfo`;

            axios.get(userinfoURL, {
                headers: {
                    'Authorization': `Bearer ${credentials.token}`
                }
            })
                .then((profile) =>
                {
                    logger.debug('received user profile');
                    credentials.profile = profile;
                    return callback();
                })
                .catch((error) =>
                {
                    logger.error(`unable to fetch user profile: ${error}`);
                    return callback(error);
                });
        }
    },
    cookie: 'vantiv-uaa-auth',
    isSecure: true,
    password: config.get('authentication.uaa.session.secret'),
    clientId: config.get('authentication.uaa.clientId'),
    clientSecret: config.get('authentication.uaa.clientSecret'),
    forceHttps: true
};

module.exports = oAuthAuthenticationConfiguration;