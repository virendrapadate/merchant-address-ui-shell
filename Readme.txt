Deployment setup
-----------------------

1) Download 
	
2) NPM install

3) Configuration-provider.ts :  Mock authentication - set to false (the last one, not the one inside if block)

4) In your Web-application : ng build --aot --prod (or ng build, only if the previous one is not working) --> This will update dist folder

5) Make sure there is not compile time error on the console. Solve before moving ahead.

6) In your Deployment app : Place all the content 'inside' the dist folder(in web app), in the public folder of Deployment Application.

