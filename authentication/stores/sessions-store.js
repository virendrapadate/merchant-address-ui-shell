'use strict';

const Promise = require('bluebird');
const uuid = require('uuid/v4');
const moment = require('moment');
const _ = require('lodash');
const logger = require('../../logging/logger');

const sessions = [];

class SessionsStore
{
    create(userData)
    {
        return Promise.try(() =>
        {
            let sessionId = uuid();

            logger.info(`creating session with sessionId ${sessionId}`);

            sessions.push({
                sessionId: sessionId,
                user: userData,
                expiresAt: moment.utc().add(30, 'minutes').toDate()
            });

            return sessionId;
        });
    }

    retrieveById(sessionId)
    {
        return Promise.try(() =>
        {
            logger.info(`finding session with sessionId ${sessionId}`);

            let matchingSession = _.find(sessions, (datum) =>
            {
                return datum.sessionId === sessionId;
            });

            if(matchingSession)
            {
                logger.info(`found session with sessionId ${sessionId}`);

                if(moment.utc(matchingSession.expiresAt).isBefore(moment.utc()))
                {
                    return null;
                }
                else
                {
                    return matchingSession.user;
                }
            }
            else
            {
                logger.info(`not able to find session with sessionId ${sessionId} out of ${sessions.length} sessions`);
            }

            return null;
        });
    }
}

module.exports = new SessionsStore();
