'use strict';

const Promise = require('bluebird');
const Bell = require('bell');
const config = require('config');

const basicAuthenticationConfiguration = require('./configurations/basic-authentication-configuration');
const oauthAuthenticationConfiguration = require('./configurations/oauth-authentication-configuration');
const cookieAuthenticationConfiguration = require('./configurations/cookie-authentication-configuration');

class AuthenticationFacade {

    configureAuthetnicationStrategies(server) {
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            /*
            Bell.simulate((request, next) =>
            {
                let simulatedProfile = {
                    email: 'me@tanmaypatel.com',
                    name: {
                        first: 'Tanmay',
                        last: 'Patel'
                    },
                    id: '__DUMMY__'
                };

                next(null, {
                    profile: simulatedProfile
                });
            });
            */
        }

        return Promise.all([
            server.register({
                register: require('hapi-auth-basic')
            })
            .then(() => {
                return server.auth.strategy('basic', 'basic', false, basicAuthenticationConfiguration);
            }),
            server.register({
                register: require('bell')
            })
            .then(() => {
                return server.auth.strategy('oauth', 'bell', false, oauthAuthenticationConfiguration);
            }),
            server.register({
                register: require('hapi-auth-cookie')
            })
            .then(() => {
                return server.auth.strategy('session', 'cookie', false, cookieAuthenticationConfiguration);
            }),
        ]);
    }
}

module.exports = new AuthenticationFacade();