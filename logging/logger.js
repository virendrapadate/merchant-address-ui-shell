const winston = require('winston');
require('winston-daily-rotate-file');

const TRANSPORTS_CONFIGURATION = [new winston.transports.Console({
    prettyPrint: true,
    colorize: true,
    level: !process.env.NODE_ENV || process.env.NODE_ENV === 'development' ? 'debug' : 'info'
})];

if(!process.env.NODE_ENV || process.env.NODE_ENV === 'development')
{
    TRANSPORTS_CONFIGURATION.push(new winston.transports.DailyRotateFile({
        filename: '.log',
        dirname: './logs',
        datePattern: 'yyyy-MM-dd',
        prepend: true,
        level: !process.env.NODE_ENV || process.env.NODE_ENV === 'development' ? 'debug' : 'info'
    }));
}

const logger = new winston.Logger({
    level: 'debug',
    colors: {
        silly: 'grey',
        debug: 'cyan',
        verbose: 'blue',
        info: 'green',
        warn: 'yellow',
        error: 'red'
    },
    transports: TRANSPORTS_CONFIGURATION
});

module.exports = logger;
