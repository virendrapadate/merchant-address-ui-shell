module.exports = {
    apps: [{
        name: 'vantiv-web-app-shell',
        script: 'index.js',
        instances: 1,
        exec_mode: 'cluster',
        env: {
        }
    }]
};
