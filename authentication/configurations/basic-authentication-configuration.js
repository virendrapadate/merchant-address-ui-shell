'use strict';

const config = require('config');
const jwt = require('jsonwebtoken');
const logger = require('../../logging/logger');


let authenticationValidator = function (request, username, password, callback) {
    logger.debug('!!!!!!! Inside authenticationValidator /');

    let validUsername = config.get('authentication.basic.username');
    let validPassword = config.get('authentication.basic.password');

    logger.debug('Stored Values - ');
    logger.debug(validUsername);
    logger.debug(validPassword);

    logger.debug('User Input Values - ');
    logger.debug(username);
    logger.debug(password);

    if (username == validUsername && password === validPassword) {
        let credentials = {
            userId: username
        };

        return callback(null, true, credentials);
    } else {
        return callback(null, false);
    }
};

const basicAuthenticationConfiguration = {
    validateFunc: authenticationValidator
};

module.exports = basicAuthenticationConfiguration;