'use strict';

const config = require('config');

const sessionsStore = require('../stores/sessions-store');
const logger = require('../../logging/logger');

let cookieValidator = function(request, session, callback)
{
    logger.debug(`validating cookie for sessionId ${session.sid}`);
    
    sessionsStore.retrieveById(session.sid)
        .then((matchingSession) =>
        {
            if(matchingSession)
            {
                logger.debug('found matching session for cookie');
                let credentials = {
                    accessToken: matchingSession.accessToken,
                    refreshToken: matchingSession.refreshToken,
                    expiresIn: matchingSession.expiresIn
                };

                return callback(null, true, credentials);
            }
            else
            {
                logger.debug('session for cookie not found');
                return callback(null, false);
            }
        })
        .catch((error) =>
        {
            logger.debug(`not able to validate cookie authentication. ${error}`);
            callback(error, false);
        });
    

    
};

const cookieAuthenticationConfiguration = {
    password: config.get('authentication.session.secret'),
    cookie: 'vantiv-session',
    redirectTo: '/login',
    isSecure: true,
    isSameSite: 'Lax',
    clearInvalid: true,
    appendNext : true,
    // ttl: 43100000,
    // keepAlive : true,
    validateFunc: cookieValidator
};

module.exports = cookieAuthenticationConfiguration;
