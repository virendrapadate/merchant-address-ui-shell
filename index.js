'use strict';

const Hapi = require('hapi');
const config = require('config');
const Promise = require('bluebird');
const moment = require('moment');
const jwt = require('jsonwebtoken');
const path = require('path');

const logger = require('./logging/logger');
const authenticationFacade = require('./authentication/authentication-facade');
const sessionStore = require('./authentication/stores/sessions-store');

global.__base = __dirname;

const SERVER_CONFIGURATION = {
    connections: {
        compression: true,
        routes: {
            cors: {
                credentials: true
            }
        }
    }
};

// create hapi server instance
const hapiServer = new Hapi.Server(SERVER_CONFIGURATION);

// create connection for api
const server = hapiServer.connection({
    host: config.get('host'),
    port: config.get('port'),
    router: {
        stripTrailingSlash: true
    },
    labels: ['api']
});

const HAPI_LOGGING_OPTIONS = {
    reporters: {
        console: [{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{
                log: '*',
                response: '*'
            }]
        }, {
            module: 'good-console'
        }, 'stdout']
    }
};

/********/
/* ROOT */
/********/

server.route({
    method: 'GET',
    path: '/heartbeat',
    handler: require('./routes/heartbeat')
});

Promise.resolve()
    .then(() => {
        return server.register({
            register: require('vision')
        });
    })
    .then(() => {
        return server.register({
            register: require('inert')
        });
    })
    .then(() => {
        return server.register({
            register: require('good'),
            options: HAPI_LOGGING_OPTIONS
        });
    })
    .then(() => {
        return server.register({
            register: require('scooter')
        });
    })
    .then(() => {
        return server.register({
            register: require('h2o2')
        });
    })
    .then(() => {
        return authenticationFacade.configureAuthetnicationStrategies(server);
    })
    .then(() => {
        /*********************/
        /* SETTING UP ROUTES */
        /*********************/

        server.route({
            method: 'GET',
            path: '/',
            config: {
                auth: {
                    mode: 'required',
                    strategies: ['basic']
                }
            },
            handler: function (request, reply) {
                logger.debug('!!!!!!! received request at /');

                logger.debug(request.auth.credentials.userId);


                reply.view('index', {
                    accessToken: request.auth.credentials.userId
                });
            }
        });

        // server.route({
        //     method: 'GET',
        //     path: '/login',
        //     config: {
        //         auth: {
        //             mode: 'required',
        //             strategies: ['basic']
        //         }
        //     },
        //     handler: function (request, reply) {
        //         logger.debug('received request at /login');

        //         if (!request.auth.isAuthenticated) {
        //             return reply(Boom.unauthorized('Authentication failed: ' + request.auth.error.message));
        //         }

        //         // check whether there’s a value for "next" query param
        //         // if not, define the default "/"
        //         const redirectURL = request.auth.credentials.query && request.auth.credentials.query.next ? request.auth.credentials.query.next : '/';

        //         sessionStore.create({
        //                 accessToken: request.auth.credentials.token,
        //                 refreshToken: request.auth.credentials.refreshToken,
        //                 expiresIn: request.auth.credentials.expiresIn
        //             })
        //             .then((sessionId) => {
        //                 request.cookieAuth.set({
        //                     sid: sessionId
        //                 });

        //                 // redirect the user to the previous page
        //                 return reply.redirect(redirectURL);

        //             });
        //     }
        // });

        // server.route({
        //     method: 'GET',
        //     path: '/logout',
        //     config: {
        //         auth: {
        //             mode: 'required',
        //             strategies: ['session']
        //         }
        //     },
        //     handler: function (request, reply) {
        //         logger.debug('received request at /logout');

        //         request.cookieAuth.clear();

        //         return reply.redirect(`${config.get('authentication.uaa.baseURL')}/logout?redirect=${config.get('authentication.uaa.redirectURL')}`);
        //     }
        // });


        server.route({
            method: 'GET',
            path: '/assets/{assetpath*}',
            handler: {
                directory: {
                    path: 'public/assets'
                }
            }
        });

        server.route({
            method: 'GET',
            path: '/build/{filename*}',
            handler: {
                directory: {
                    path: 'public/build'
                }
            }
        });

        server.route({
            method: 'GET',
            path: '/{filename*}',
            config: {
                auth: {
                    mode: 'required',
                    strategies: ['basic']
                }
            },
            handler: function (request, reply) {
                logger.debug('received request at /{filename*}');
                const filename = request.path;
                const fileExtension = path.extname(request.path);

                logger.debug(filename);
                logger.debug(fileExtension);

                if (fileExtension && fileExtension.length) {
                    return reply.file('public' + filename);
                }

                reply.view('index', {
                    accessToken: request.auth.credentials.accessToken
                });
            }
        });

        return Promise.all([]);
    })
    .then(() => {
        server.views({
            engines: {
                html: require('handlebars')
            },
            path: __dirname + '/public'
        });
    })
    .then(() => {
        return hapiServer.start();
    })
    .then(() => {
        logger.info(`server running at: ${server.info.uri}`);
    })
    .catch((error) => {
        logger.error(error);
        throw error;
    });


process.on('SIGINT', () => {
    logger.info('stopping server');

    hapiServer.stop()
        .then((error) => {
            logger.info('server stopped');
            process.exit((error) ? 1 : 0);
        });
});

process.on('unhandledRejection', (reason, promise) => {
    logger.error('unhandled rejection,');
    logger.error(`promise: ${promise}`);
    logger.error(`reason: ${reason}`);

    throw reason;
});

process.on('uncaughtException', (error) => {
    logger.error('uncaught exception,');
    logger.error(error);
});

process.on('warning', (warning) => {
    logger.warn('got following warning,');
    logger.warn(warning.name);
    logger.warn(warning.message);
    logger.warn(warning.stack);
});

process.on('exit', (code) => {
    logger.info(`application is about to exit with code ${code}`);
});